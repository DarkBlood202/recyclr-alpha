from os import path
from random import randint

from PIL import Image
import noise
import numpy as np

from settings import TEMP_FOLDER, MAPS_FOLDER
from functions import multi_compare


class World:
    def __init__(
        self,
        conditions,
        colors,
        shape=(1024, 1024),
        offset=randint(5, 100),
        scale=100.0,
        persistence=0.5,
        lacunarity=2.0,
        octaves=6,
    ):
        self.shape = shape
        self.offset = offset
        self.conditions = conditions
        self.colors = colors
        self.scale = scale
        self.persistence = persistence
        self.lacunarity = lacunarity
        self.octaves = octaves

    # generate world data
    def create(self):
        self.world_data = generate_world_data(
            self.shape,
            self.scale,
            self.octaves,
            self.persistence,
            self.lacunarity,
            self.offset,
        )
        self.color_world = colorize(
            self.world_data, self.shape, self.conditions, self.colors
        )
        print(f"World [{self.shape[1]}x{self.shape[0]} @ {self.offset}] generated.")

    # save colored world to image file
    def save_image(self):
        im = Image.fromarray(self.color_world, "RGB")
        im.save(path.join(TEMP_FOLDER, "world.png"))
        print("World saved.")


# map areas to tile idxs
def world_to_tile(arr, shape, condition_array):
    tile_world = np.zeros(shape, dtype="uint8")

    for i in range(shape[0]):
        for j in range(shape[1]):
            tile_value = multi_compare(arr[i][j], condition_array)
            tile_world[i][j] = tile_value

    return tile_world


# save tiled world to txt file
def save_tile_file(tile_world, filename):
    np.savetxt(filename, tile_world, delimiter=" ", fmt="%d")


def generate_world_data(shape, scale, octaves, persistence, lacunarity, base):
    # Generating world array
    world = np.zeros(shape)

    # Filling array with noise
    for i in range(shape[0]):
        for j in range(shape[1]):
            world[i][j] = noise.pnoise2(
                j / scale,
                i / scale,
                octaves=octaves,
                persistence=persistence,
                lacunarity=lacunarity,
                base=base,
            )

    return world


# function to define a color based on -1 to 1 value
def colorize(arr, shape, condition_array, color_array):
    color_world = np.zeros(arr.shape + (3,), dtype="uint8")

    for i in range(shape[0]):
        for j in range(shape[1]):
            idx_value = multi_compare(arr[i][j], condition_array)
            color_world[i][j] = color_array[idx_value]

    return color_world


def read_map_data(map_file):
    with open(path.join(MAPS_FOLDER, map_file)) as map:
        map_data = [
            [int(c) for c in row] for row in map.read().replace(" ", "").split("\n")
        ]

    return map_data
