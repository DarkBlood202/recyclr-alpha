import pygame as pg


class Button(pg.sprite.Sprite):
    def __init__(self, x, y, color) -> None:
        super().__init__()
        self.image = pg.Surface((32, 32))
        self.image.fill(color)
        self.rect = self.image.get_rect(center=(x, y))
        self.triggered = False

    def on_hover(self):
        self.image.fill("white")
        # print("hovered!")

    def on_action(self):
        self.image.fill("green")
        # print("click!")
