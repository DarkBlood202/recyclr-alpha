class Entity:
    def __init__(self):
        self.cost = 0
        self.obsoletion = 0
        self.generation_rate = 0
        self.obsoletion_rate = 0
        self.detox_rate = 0
        self.needs_power = False
        self.upgrades = 0


class MainBuilding(Entity):
    def __init__(self):
        super().__init__()
        self.generation_rate = 1


class Extensor(Entity):
    def __init__(self):
        super().__init__()
        self.cost = 300
        self.obsoletion_rate = 1
        self.needs_power = True
