import sys
from os import environ as env
from os.path import join as jn

from map import Map

env["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"

import pygame as pg

from settings import *
from world import World, read_map_data, world_to_tile, save_tile_file
import vfx
from ui import Button


class Game:
    def __init__(self):
        # initializing pygame and setting up display
        pg.init()
        pg.display.set_caption(TITLE)
        self.screen = pg.display.set_mode(WINDOW_SIZE)  # scales over
        # self.viewport = pg.Surface(SCALE_1_WINDOW_SIZE)  # scale 1

        self.clock = pg.time.Clock()

    def load_res(self):
        # load tiles
        sand = pg.image.load(jn(TILES_FOLDER, "sand.png")).convert_alpha()
        dirt = pg.image.load(jn(TILES_FOLDER, "dirt.png")).convert_alpha()
        grass = pg.image.load(jn(TILES_FOLDER, "grass.png")).convert_alpha()
        water = pg.image.load(jn(TILES_FOLDER, "water.png")).convert_alpha()
        mineral = pg.image.load(jn(TILES_FOLDER, "mineral.png")).convert_alpha()
        flesh = pg.image.load(jn(TILES_FOLDER, "flesh.png")).convert_alpha()

        # setting up tiles array
        self.tile_types = [sand, dirt, grass, water, mineral, flesh]

        # initializing surfaces
        self.tile_surf = pg.Surface((64, 64), pg.SRCALPHA)

    def new_game(self):
        # generate new map data
        # conditions = [-0.05, 0.0, 1.0]
        # colors = [
        # list(Color.HOOKERS.value),
        # list(Color.BEAVER.value),
        # list(Color.RUSSIAN.value),
        # ]

        # world = World(conditions, colors, persistence=0.55, shape=(24, 24))
        # world.create()
        # world.save_image()

        # Saving map to a txt file
        # tile_world = world_to_tile(world.world_data, world.shape, conditions)
        # save_tile_file(tile_world, "rummage.rcm")

        # load map data
        map_data = read_map_data("line.rcm")
        map_h = len(map_data)
        map_w = len(map_data[0])
        self.map = Map(map_w, map_h, map_data)

        # load map vfx
        self.vfx_stars = []
        for i in range(NUMBER_OF_STARS):
            self.vfx_stars.append(vfx.Star())

        self.ui = pg.sprite.Group()
        # b = Button(32, 32, "red")
        # b.add(self.ui)

        self.cam_offset = pg.math.Vector2()
        self.cam_speed = 5

    def run(self):
        self.playing = True

        while self.playing:
            self.dt = self.clock.tick(FPS) / 1000

            self.events()
            self.update()
            self.draw()

    def quit(self):
        pg.quit()
        sys.exit()

    def events(self):
        for ev in pg.event.get():
            # closing game via X button
            if ev.type == pg.QUIT:
                self.quit()

    def update(self):
        # get mouse position and button states
        mouse = pg.mouse.get_pos()
        mouse_buttons = pg.mouse.get_pressed()

        # get pressed keys
        keys = pg.key.get_pressed()

        # if mouse in collision with ui elements
        for e in self.ui.sprites():
            if e.rect.collidepoint(mouse):
                e.on_hover()
                if mouse_buttons[0] and not e.triggered:
                    e.on_action()
                    e.triggered = True
                elif not mouse_buttons[0]:
                    e.triggered = False
            else:
                e.triggered = False

        # [[debugging or sth]]
        print(f"Mouse: {mouse}")
        print(f"Offset: {self.cam_offset}")
        print(
            f"Mouse + offset: ({mouse[0] - self.cam_offset.x}, {mouse[1] - self.cam_offset.y})"
        )

        ######################

        # get continous key press
        if keys[pg.K_a] or keys[pg.K_LEFT]:
            self.cam_offset.x += self.cam_speed
        elif keys[pg.K_d] or keys[pg.K_RIGHT]:
            self.cam_offset.x -= self.cam_speed

        if keys[pg.K_w] or keys[pg.K_UP]:
            self.cam_offset.y += self.cam_speed
        elif keys[pg.K_s] or keys[pg.K_DOWN]:
            self.cam_offset.y -= self.cam_speed

    def draw(self):
        # fill pixels with black
        self.screen.fill("black")

        # draw vfx elements
        for star in self.vfx_stars:
            star.update()
            star.show(self.screen)

        # draw tiles from map data
        for row in self.map.tiles:
            for tile in row:
                if tile.terrain != 0:
                    self.screen.blit(
                        self.tile_types[tile.terrain - 1],
                        (
                            self.cam_offset.x + tile.x * 32 - tile.y * 32,
                            self.cam_offset.y + tile.x * 16 + tile.y * 16,
                        ),
                    )

        # draw ui to screen
        self.ui.draw(self.screen)

        # update the display
        pg.display.flip()

    def main_menu(self):
        pass


if __name__ == "__main__":
    g = Game()
    g.load_res()
    g.main_menu()

    while True:
        g.new_game()
        g.run()
