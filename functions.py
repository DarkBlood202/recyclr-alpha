import pygame as pg


# Load sprites to list from a spritesheet
def sprites_from_sheet(sheet, start, size, columns, rows):
    sprites = []

    for j in range(rows):
        for i in range(columns):
            location = (start[0] + size[0] * i, start[1] + size[1] * j)
            sprites.append(sheet.subsurface(pg.Rect(location, size)))

    return sprites


# defining multi-compare function
def multi_compare(value, condition_array):
    for idx, condition in enumerate(condition_array):
        if value < condition:
            # print(value, condition, idx)
            return idx


# Clamp
def clamp(value, mini, maxi):
    return max(min(value, maxi), mini)


# Remap values
def remap(value, left_min, left_max, right_min, right_max):
    left_span = left_max - left_min
    right_span = right_max - right_min

    value_scaled = (value - left_min) / (left_span)

    return right_min + (value_scaled * right_span)
