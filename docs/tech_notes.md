## Main Menu Structure

```
> Start Game
    + Normal Game
    + Custom Game

> Map Editor

> Options
    - Window Size / Resolution
    - BGM Volume
    - SFX Volume
    
> Extras
	- Music room

> Quit
```

## Game flow

![](./flow.png)

## Saving data format

| re_Cyclr matrix data (.rcm)                                  | re_Cyclr map data (.rcx)                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Contains only tile position data.                            | Contains tile position data, state of each tile and associated entity. |
| Maximum compatibility as data is stored as a integer matrix. | Poor (or non-existent) compatibility as data is stored in a custom format. |

~~~
TILE:
==========
- Position
	+ x
	+ y

- Terrain

- Status
	+ toxic ?
	+ powered ?
	+ hovered ?
	
- Entity
~~~

