# Re_Cyclr

Re_Cyclr is a strategy game based on how well you can clean a contaminated land  by taking control of two different races. Each land will be unique and require a new strategy to succeed.

## Gameplay

You'll spawn in a contaminated land with your initial structure. This structure generates material overtime, which you will use to build more structures to expand your base and reach more areas within the land.

Every land's atmosphere is highly toxic, it will gradually damage your structures over time. Your main goal is to detoxify as much areas as you can before leaving. Large detoxified areas will become sustainable ecosystems.

### Land types

| Land type | Description                                                  |
| --------- | ------------------------------------------------------------ |
| Trrum     | Land mainly composed by water, dirt, sand and grass.         |
| Desertum  | Dry land covered almost entirely by dirt and sand. Mineral rocks can be found in some areas. |
| Tal'assum | Mostly flooded land with very small areas of vegetation.     |
| Organicum | Land infested with bio-matter of some sort on top of dirt and rock. |
| Alunum    | Cold land made of different mineral rocks. Microorganisms have been detected to live on the surface as proof life is viable somehow. |
| Collapsum | Very rare land of variable distribution filled by gaps of different sizes. |

### Time

You'll be given a certain amount of maximum time to perform your cleaning operation before your structures collapse due to the harsh conditions. You can always return before the timer ends, but this will end the mission as well.

### Ecosystem %

As areas are being detoxified, there is a chance ecosystems start spawning inside. The more detoxified areas, the more ecosystems will appear. You want to have as much of these as possible before leaving.

## Races

### Race 1

#### The Facilities

The Facilities is the operation center of the race 1 and where most of the critical detoxification, decomposition and rekindling processes take place. The facilities have their own energy network, that allows machines to be powered within an extensible radius. 

#### Machines

Machines are independent upgradeable structures. Machines can be built separately as long as they're inside an energy network, but they take time to initialize.

Machines decay over time depending on the contamination conditions of the land and  can be rekindled as material once they become obsolete. Machines can also start failing, which diminishes their performance and require a restart to go back to normal.

| Name               | Description                                                  | Cost |
| ------------------ | ------------------------------------------------------------ | ---- |
| Energetic Extensor | Allows you to expand your current energy network across the land. | 300  |
| Land Purifier      | Generates materials from nearby contaminated areas and renders them detoxified. | 450  |

#### Spells

| Name           | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| Nanobots Swarm | Send a swarm of repairing nanobots towards the desired location, preventing a structure from suffering any damage for a limited period of time. |

### The Morphes

#### The Core

The Core is a primitive being that carries the ADN of all Morphes' evolutionary tree. It is able to survive harsh conditions autonomously, digesting the few nutritional elements within waste and remains. Such metabolic processes leave the Core essentially sessile to keep it alive. 

#### Organisms

All Morphes' organisms originate from the core as specialized appendices, therefore, growing multiple takes significantly longer. However, once they grow, they are ready immediately.

Organisms can grow over time, which increases their capabilities and effectiveness.

Organisms decay over time depending on the contamination conditions of the land, specially faster if they somehow lose connection with the core, as it's how these structures are kept "alive".

| Name                | Description                                                  | Cost |
| ------------------- | ------------------------------------------------------------ | ---- |
| Embryonic Cotyledon | Allows other organisms to grow from it, including others of its type. | 250  |
| Lymphatic Bulb      | Generates materials from nearby contaminated areas and renders them detoxified. | 375  |

#### Spells

| Name            | Description                                                  |
| --------------- | ------------------------------------------------------------ |
| Adrenaline Rush | Secretes adrenaline to prevent a structure from dying during a limited period of time and greatly increasing the target's performance depending on how damaged it is (maximum at 1%). |

### Differences

| Race 1                                                       | Morphes                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Each upgrade point requires investigation.                   | Upgrades are automatic as organisms "grow".                  |
| Machines can be built anywhere, as long as they are inside an energy network. | Organisms do not require an energy network to grow, but they have to be connected to the core. |
| Obsolete machines can be rekindled as material at any moment, leaving the area ready to use. | Decayed organisms require an extra time to disappear and leave the area ready to use again. |

