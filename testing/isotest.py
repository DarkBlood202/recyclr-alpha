import sys
import pygame as pg

pg.init()
screen = pg.display.set_mode((512, 288), 0, 32)
viewport = pg.Surface((512, 288))

sand_img = pg.image.load("assets/sand.png").convert_alpha()
dirt_img = pg.image.load("assets/dirt.png").convert_alpha()
grass_img = pg.image.load("assets/grass.png").convert_alpha()
water_img = pg.image.load("assets/water.png").convert_alpha()
mineral_img = pg.image.load("assets/mineral.png").convert_alpha()
flesh_img = pg.image.load("assets/flesh.png").convert_alpha()

tile_surf = pg.Surface((64, 64), pg.SRCALPHA)

with open("worldata.txt") as f:
    map_data = [[int(c) for c in row] for row in f.read().split("\n")]

while True:
    viewport.fill("black")

    for y, row in enumerate(map_data):
        for x, tile in enumerate(row):
            if tile == 1:
                tile_surf = sand_img
            elif tile == 2:
                tile_surf = dirt_img
            elif tile == 3:
                tile_surf = grass_img
            elif tile == 4:
                tile_surf = water_img
            elif tile == 5:
                tile_surf = mineral_img
            elif tile == 6:
                tile_surf = flesh_img

            if tile != 0:
                pg.draw.rect(viewport, "white", pg.Rect(x * 8, y * 8, 8, 8), 1)
                viewport.blit(tile_surf, (225 + x * 32 - y * 32, 0 + x * 16 + y * 16))

    for ev in pg.event.get():
        if ev.type == pg.QUIT:
            pg.quit()
            sys.exit()
        if ev.type == pg.KEYDOWN:
            if ev.key == pg.K_ESCAPE:
                pg.quit()
                sys.exit()

    screen.blit(pg.transform.scale(viewport, screen.get_size()), (0, 0))
    # screen.blit(viewport, (0, 0))

    pg.display.flip()
