import json

tiledata = {
    i: {j: {k: {"terrain": 0} for k in range(8)} for j in range(8)} for i in range(8)
}

with open("testmap.rcmap", "w") as f:
    f.write(json.dumps(tiledata))

print("Generated map file")
