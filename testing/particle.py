import sys, random
import pygame as pg

pg.init()
pg.display.set_caption("Particle test")
screen = pg.display.set_mode((500, 500), 0, 32)
clock = pg.time.Clock()

# Particles structure: [location, velocity, timer]
particles = []

while True:
    screen.fill((0, 0, 0))

    particles.append(
        [[250, 250], [random.randint(0, 20) / 10 - 1, -2], random.randint(4, 6)],
        # [
        #     # [pg.mouse.get_pos()[0], pg.mouse.get_pos()[1]],
        #     [random.randint(0, 500), random.randint(0, 500)],
        #     [random.randint(0, 20) / 10 - 1, random.randint(-1, 1)],
        #     2,
        # ],
    )

    for p in particles:
        p[0][0] += p[1][0]
        p[0][1] += p[1][1]
        p[2] -= 0.1
        p[1][1] += 0.03

        pg.draw.circle(screen, (255, 255, 255), (int(p[0][0]), int(p[0][1])), int(p[2]))

        if p[2] <= 0:
            particles.remove(p)

    print(f"Particles: {len(particles)}")

    for ev in pg.event.get():
        if ev.type == pg.QUIT:
            pg.quit()
            sys.exit()

        if ev.type == pg.KEYDOWN:
            if ev.key == pg.K_ESCAPE:
                pg.quit()
                sys.exit()

    pg.display.flip()
    clock.tick(60)
