import pickle


class Tile:
    def __init__(
        self,
        x,
        y,
        terrain,
        is_toxic=True,
        is_powered=False,
        is_hovered=False,
        entity=None,
    ):
        self.x = x
        self.y = y
        self.terrain = terrain

        self.is_toxic = is_toxic
        self.is_powered = is_powered
        self.is_hovered = is_hovered

        self.entity = entity

    def info(self):
        return f"Tile ({self.x},{self.y}) - Type: {self.terrain} - [{self.is_toxic}][{self.is_powered}][{self.is_hovered}] - {self.entity}"


if __name__ == "__main__":
    t1 = Tile(0, 0, 1)
    t2 = Tile(12, 10, 3)

    print(t1.info(), t2.info())

    with open("save_test", "wb") as f:
        pickle.dump(t1, f)

    with open("save_test", "rb") as f:
        t2 = pickle.load(f)

    print(t1.info(), t2.info())
