import pickle

import numpy as np
import pygame as pg

from world import read_map_data


class Tile(pg.sprite.Sprite):
    def __init__(self, x, y, terrain, entity=None):
        super().__init__()
        self.x = x
        self.y = y
        self.terrain = terrain
        self.entity = entity


class Map:
    def __init__(self, w, h, tiles):
        self.size = (h, w)
        self.tiles = np.empty(self.size, dtype="object")

        for y, row in enumerate(tiles):
            for x, tile in enumerate(row):
                self.tiles[y][x] = Tile(x, y, tile)
