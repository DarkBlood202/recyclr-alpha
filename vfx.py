from random import randint, choice

import pygame as pg

from functions import remap

# from settings import SCALE_1_WINDOW_SIZE, PALETTE, WINDOW_SIZE
from settings import PALETTE, WINDOW_SIZE

# WIDTH = SCALE_1_WINDOW_SIZE[0]
# HEIGHT = SCALE_1_WINDOW_SIZE[1]

WIDTH = WINDOW_SIZE[0]
HEIGHT = WINDOW_SIZE[1]


class Star:
    def __init__(self):
        self.x = randint(-WIDTH, WIDTH)
        self.y = randint(-HEIGHT, HEIGHT)
        self.z = randint(0, WIDTH)

        self.pz = self.z

        self.speed = 5
        self.color = choice([PALETTE["goldenlight"], PALETTE["chalice"]])

    def update(self):
        self.z -= self.speed
        if self.z < 1:
            self.z = WIDTH
            self.x = randint(-WIDTH, WIDTH)
            self.y = randint(-HEIGHT, HEIGHT)
            self.pz = self.z

    def show(self, screen):
        sx = int(remap(self.x / self.z, 0, 1, 0, WIDTH))
        sy = int(remap(self.y / self.z, 0, 1, 0, HEIGHT))

        px = int(remap(self.x / self.pz, 0, 1, 0, WIDTH))
        py = int(remap(self.y / self.pz, 0, 1, 0, HEIGHT))

        self.pz = self.z
        pg.draw.line(
            screen,
            self.color,
            (px + WIDTH // 2, py + HEIGHT // 2),
            (sx + WIDTH // 2, sy + HEIGHT // 2),
        )
