from random import randint
import sys
from os.path import join as jn
from os import environ as env
from os import mkdir

env["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"

import pygame as pg

from .settings import *
from world import World, world_to_tile, save_tile_file


class Game:
    def __init__(self):
        pg.init()
        pg.display.set_caption(TITLE)
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        print(f"Window size: {WIDTH}x{HEIGHT} (scale: x{SCALING_FACTOR})")
        self.clock = pg.time.Clock()

    def new_game(self):
        # Setting up conditions for a new map
        conditions = [-0.05, 0.0, 1.0]
        colors = [
            list(Color.HOOKERS.value),
            list(Color.BEAVER.value),
            list(Color.RUSSIAN.value),
        ]

        # Generating a new map
        world = World(conditions, colors, scale=200.0, persistence=0.55)
        world.create()
        world.save_image()

        # Saving map to a txt file
        tile_world = world_to_tile(world.world_data, (1024, 1024), conditions)
        save_tile_file(tile_world)

        # Load map image
        bg = pg.image.load(jn(TEMP_FOLDER, "world.png")).convert()
        bg_size = bg.get_size()

        # Scale map image
        bg_scaled = (bg_size[0] * SCALING_FACTOR, bg_size[1] * SCALING_FACTOR)
        self.bg = pg.transform.scale(bg, bg_scaled)

        # Create viewport for map
        self.viewport = pg.Surface((HEIGHT, HEIGHT))

        # Initialize cursor for map navigation
        self.area_cursor = randint(0, TILENUM**2)
        print(f"You start at ({self.area_cursor % 8},{self.area_cursor // 8})")

        # Initialize map data
        self.world_data = {
            i: {
                j: {
                    k: {"tox": True, "pow": False, "ent": None, "hov": False}
                    for k in range(TILENUM)  # x
                }
                for j in range(TILENUM)  # y
            }
            for i in range(TILENUM**2)  # areas
        }

        # Pre-defining surfaces for cell states
        self.tox_surf = pg.Surface((CELL_SIZE, CELL_SIZE))
        self.tox_surf.fill(Color.LAVENDER.value)

        self.pow_surf = pg.Surface((CELL_SIZE, CELL_SIZE), pg.SRCALPHA).convert_alpha()
        self.pow_color = (228, 206, 180, 128)
        self.pow_surf.fill(self.pow_color)

        self.hov_surf = self.pow_surf.copy()
        self.hov_color = (255, 255, 255, 32)
        self.hov_surf.fill(self.hov_color)

    def run(self):
        self.playing = True

        while self.playing:
            self.dt = self.clock.tick(FPS) / 1000

            self.events()
            self.update()
            self.draw()

    def quit(self):
        pg.quit()
        sys.exit()

    def events(self):
        for event in pg.event.get():
            # Closing the game via X button
            if event.type == pg.QUIT:
                self.quit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    self.quit()

                # Moving around the map areas
                elif event.key == pg.K_w or event.key == pg.K_UP:
                    if self.area_cursor >= 0 + 8:
                        self.area_cursor -= 8

                elif event.key == pg.K_a or event.key == pg.K_LEFT:
                    if self.area_cursor > 0 and self.area_cursor % 8 != 0:
                        self.area_cursor -= 1

                elif event.key == pg.K_s or event.key == pg.K_DOWN:
                    if self.area_cursor <= 63 - 8:
                        self.area_cursor += 8

                elif event.key == pg.K_d or event.key == pg.K_RIGHT:
                    if self.area_cursor < 63 and self.area_cursor % 8 != 7:
                        self.area_cursor += 1

    def update(self):
        # Get cells from current selected area
        self.current_cells = self.world_data[self.area_cursor]

        # Reset hover status for every cell (might need optimization)
        for i in range(TILENUM**2):
            for j in range(TILENUM):
                for k in range(TILENUM):
                    self.world_data[i][j][k]["hov"] = False

        # Get active cell
        mx, my = pg.mouse.get_pos()
        if mx >= VIEWPORT_OFFSET and mx < WIDTH - VIEWPORT_OFFSET:
            cell_x = (mx - VIEWPORT_OFFSET) // CELL_SIZE
            cell_y = my // CELL_SIZE

            self.world_data[self.area_cursor][cell_y][cell_x]["hov"] = True

    def draw(self):
        # Fill window
        self.screen.fill(Color.SPACE.value)

        # Draw map to viewport
        map_offset_x = 0 - int(self.area_cursor % TILENUM) * HEIGHT
        map_offset_y = 0 - int(self.area_cursor / TILENUM) * HEIGHT
        self.viewport.blit(self.bg, (map_offset_x, map_offset_y))

        # Draw cell status on viewport
        for row, i in self.current_cells.items():
            for col, cell in i.items():
                if cell["tox"]:
                    self.viewport.blit(
                        self.tox_surf,
                        (col * CELL_SIZE, row * CELL_SIZE),
                        special_flags=pg.BLEND_MIN,
                    )
                if cell["ent"]:
                    pass
                if cell["pow"]:
                    self.viewport.blit(
                        self.pow_surf,
                        (col * CELL_SIZE, row * CELL_SIZE),
                    )
                if cell["hov"]:
                    self.viewport.blit(
                        self.hov_surf, (col * CELL_SIZE, row * CELL_SIZE)
                    )

        # Draw viewport on screen
        self.screen.blit(self.viewport, (VIEWPORT_OFFSET, 0))

        # Refresh the window
        pg.display.flip()

    def main_menu(self):
        pass


if __name__ == "__main__":
    try:
        mkdir(TEMP_FOLDER)
    except:
        print("Retrieving temp folder...")

    g = Game()
    g.main_menu()
    while True:
        g.new_game()
        g.run()
