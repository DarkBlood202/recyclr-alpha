from os import path
from enum import Enum


# Options
WINDOW_SIZE = (1024, 576)
# SCALE_1_WINDOW_SIZE = (512, 288)

# VFX
NUMBER_OF_STARS = 500

# Game params
FPS = 60
TITLE = "Re_Cyclr alpha"


# Directories
GAME_FOLDER = path.dirname(__file__)
ASSETS_FOLDER = path.join(GAME_FOLDER, "assets")
TILES_FOLDER = path.join(ASSETS_FOLDER, "tiles")
MAPS_FOLDER = path.join(GAME_FOLDER, "maps")
TEMP_FOLDER = path.join(GAME_FOLDER, "temp")

# Color constants
class Color(Enum):
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)

    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)

    SPACE = (33, 34, 54)
    CATAWBA = (115, 48, 62)
    LAVENDER = (121, 78, 107)
    HOOKERS = (82, 122, 118)
    RUSSIAN = (109, 133, 96)
    BEAVER = (163, 134, 107)
    CAMEL = (201, 156, 105)
    SILVER = (208, 196, 189)


PALETTE = {
    "goldenlight": (254, 254, 215),
    "tan": (219, 188, 150),
    "goldenrod": (221, 172, 70),
    "byzantium": (104, 61, 100),
    "bronze": (156, 102, 89),
    "cordovan": (136, 67, 79),
    "mauve": (77, 40, 49),
    "chalice": (169, 171, 163),
    "granite": (102, 104, 105),
    "maxib": (81, 177, 202),
    "french": (23, 115, 184),
    "may": (99, 159, 91),
    "spring": (55, 110, 73),
    "jet": (50, 52, 65),
    "space": (34, 32, 52),
    "black": (0, 0, 0),
}
